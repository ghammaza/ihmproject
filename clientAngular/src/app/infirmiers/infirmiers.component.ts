import { Component, Input, OnInit } from '@angular/core';
import {PatientInterface} from '../dataInterfaces/patient';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';

@Component({
  selector: 'app-infirmiers',
  templateUrl: './infirmiers.component.html',
  styleUrls: ['./infirmiers.component.scss']
})
export class InfirmiersComponent implements OnInit {
  private _cms: CabinetInterface;
  public get cms(): CabinetInterface { return this._cms; }

  constructor(cabinetMedicalService: CabinetMedicalService ) {

    this.initCabinet(cabinetMedicalService);

  }

  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    console.log( this.cms );
  }

  ngOnInit() {
  }
  getNom(item){

    return item.nom + "  "+item.prénom;
  }
  getUrlP(item){
    return "url(/data/"+item.photo+")";
  }
}
