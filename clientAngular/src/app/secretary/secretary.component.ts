import { CabinetMedicalService } from './../cabinet-medical.service';
import { Component, OnInit } from '@angular/core';
import { CabinetInterface } from '../dataInterfaces/cabinet';
import {InfirmierInterface} from '../dataInterfaces/infirmier';
import {PatientInterface} from '../dataInterfaces/patient';

@Component({
  selector: 'app-secretary',
  templateUrl: './secretary.component.html',
  styleUrls: ['./secretary.component.scss']
})
export class SecretaryComponent implements OnInit {
 imageSources : string[]=["/assets/images/ordonnancier-infirmier_1_orig.jpg",
   "/assets/images/koko.png",
   "/assets/images/images.png",
   "/assets/images/mag.png",
   "/assets/images/med.png",
   "/assets/images/hhh.png"];
  private cab: CabinetInterface;
  index:number;
  public get cb(): CabinetInterface { return this.cab; }
  constructor(cabinetMedicalService: CabinetMedicalService) {
    this.initCabinet(cabinetMedicalService); }


  async initCabinet(cabinetMedicalService) {
    this.cab = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    console.log( this.cb );
  }
  ngOnInit() {
  }
  getNum(item){
    return item.numéro;
  }
  getET(item){
    return item.étage;

  }


}
