import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecretaryComponent } from './secretary/secretary.component';
import { InfirmiersComponent } from './infirmiers/infirmiers.component';
import { PatientComponent } from './patient/patient.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyOwnCustomMaterialModule } from './material';
import {RouterModule, Routes} from '@angular/router';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { EditPatientComponent } from './edit-patient/edit-patient.component';
import { SearchComponent } from './search/search.component';

const appRoutes : Routes =[
  {path:'infirmieres' , component:InfirmiersComponent},
  {path:'patients',component:PatientComponent},
  {path:'patients/:numéroSécuritéSociale',component:EditPatientComponent},
  {path:'search/:numéroSécuritéSociale',component:EditPatientComponent},
  {path:'addPat',component:AddPatientComponent},
  {path:'search',component:SearchComponent},
  {path:'',component:SecretaryComponent},
  {path:'notFound',component:NotFoundComponent},
  {path:'**',redirectTo:'notFound'}
];
@NgModule({
  declarations: [
    AppComponent,
    SecretaryComponent,
    InfirmiersComponent,
    PatientComponent,
    AddPatientComponent,
    NotFoundComponent,
    EditPatientComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MyOwnCustomMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes,{onSameUrlNavigation: 'reload'})
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
