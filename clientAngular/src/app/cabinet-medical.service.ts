//https://glitch.com/edit/#!/l3m-projet

import { Adresse } from './dataInterfaces/adresse';
import { InfirmierInterface } from './dataInterfaces/infirmier';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CabinetInterface } from './dataInterfaces/cabinet';
import { PatientInterface } from './dataInterfaces/patient';
import { sexeEnum } from './dataInterfaces/sexe';
import {reject, resolve} from 'q';
import {promise} from 'selenium-webdriver';

@Injectable({
  providedIn: 'root'
})
export class CabinetMedicalService {

  public _cabinet: CabinetInterface;
  public patientsRes : PatientInterface[];
  private _http: HttpClient;
  public get http(): HttpClient { return this._http; }

  constructor( http: HttpClient ) {
    this._http = http;
  }

  async getData( url: string ): Promise<CabinetInterface>
  {
    //get HTTP response as text
    const response = await this.http.get(url, { responseType: 'text' }).toPromise();

    //parse the response with DOMParser
    let parser = new DOMParser();
    let doc = parser.parseFromString(response, "application/xml");

    //if no doc, exit
    if(!doc) return null;

    //default cabinet
    const cabinet: CabinetInterface = {
      infirmiers: [],
      patientsNonAffectés: [],
      patients: [],
      adresse: this.getAdressFrom( doc.querySelector( "cabinet" ) )
    };

    // 1 - tableau des infirmiers
    const infirmiersXML =  Array.from( doc.querySelectorAll( "infirmiers > infirmier" ) ); //transformer la NodeList en tableau pour le map

    cabinet.infirmiers = infirmiersXML.map( I => ({
      id      : I.getAttribute("id"),
      prénom  : I.querySelector("prénom").textContent,
      nom     : I.querySelector("nom"   ).textContent,
      photo   : I.querySelector("photo" ).textContent,
      adresse : this.getAdressFrom(I),
      patients: []
    }) );
    const patients1XML =  Array.from( doc.querySelectorAll( "patients > patient" ) ); //transformer la NodeList en tableau pour le map

    cabinet.patients = patients1XML.map( I => ({
      prénom  : I.querySelector("prénom").textContent,
      nom     : I.querySelector("nom"   ).textContent,
      sexe: I.querySelector("sexe").textContent === "M" ? sexeEnum.M : sexeEnum.F,
      numéroSécuritéSociale: I.querySelector("numéro").textContent,
      adresse: this.getAdressFrom( I ),
      visiteDate: I.querySelector( "visite[date]" ).getAttribute("date")
    }) );

    // 2 tableau des patients
    /*const patientsXML  = Array.from( doc.querySelectorAll( "patients > patient" ) );
    const patients: PatientInterface[] = patientsXML.map( P => ({
      prénom: P.querySelector("prénom").textContent,
      nom: P.querySelector("nom").textContent,
      sexe: P.querySelector("sexe").textContent === "M" ? sexeEnum.M : sexeEnum.F,
      numéroSécuritéSociale: P.querySelector("numéro").textContent,
      adresse: this.getAdressFrom( P )
    }) );*/

    // 3 Tableau des affectations à faire.
    const affectations = patients1XML.map( (P, i) => {
      const visiteXML = P.querySelector( "visite[intervenant]" );
      let infirmier: InfirmierInterface = null;
      if (visiteXML !== null) {
        infirmier = cabinet.infirmiers.find( I => I.id === visiteXML.getAttribute("intervenant") );
      }
      return {patient: cabinet.patients[i], infirmier: infirmier};
    } );

    // 4 Réaliser les affectations
    affectations.forEach( ({patient: P, infirmier: I}) => {
      if (I !== null) {
        I.patients.push( P );
      } else {
        cabinet.patientsNonAffectés.push( P );
      }
    });

    // Return the cabinet
    this._cabinet = cabinet;
    return cabinet;

  }

  private getAdressFrom(root: Element): Adresse {
    let node: Element;
    return {
      ville       : (node = root.querySelector("adresse > ville")     ) ? node.textContent                    : "",
      codePostal  : (node = root.querySelector("adresse > codePostal")) ? parseInt(node.textContent, 10) : 0,
      rue         : (node = root.querySelector("adresse > rue")       ) ? node.textContent                    : "",
      numéro      : (node = root.querySelector("adresse > numéro")    ) ? node.textContent                    : "",
      étage       : (node = root.querySelector("adresse > étage")     ) ? node.textContent                    : "",
    };
  }
  /*---------------------------verfier l'existance ou pas d'un patient a ajouter*/
  public dontExiste(patient :PatientInterface) : boolean{
    let i : number = 0;

    while (this._cabinet.patients.length >i){
      if(this._cabinet.patients[i].nom === patient.nom && this._cabinet.patients[i].prénom === patient.prénom
      || this._cabinet.patients[i].numéroSécuritéSociale === patient.numéroSécuritéSociale){
        return false;
      }
      i++;
    }
    return true;
      }
      /*---------------si le patient existe on envoie l'un des deux message */
      patientExiste(patient :PatientInterface) : string{
        let i : number = 0;

        while (this._cabinet.patients.length >i){
          if(this._cabinet.patients[i].nom == patient.nom && this._cabinet.patients[i].prénom == patient.prénom){
            return "le nom et le prénom que vous venez de saisir existe deja";
          }
          if(this._cabinet.patients[i].numéroSécuritéSociale == patient.numéroSécuritéSociale ){
            return " ce numéro de  Sécurité Sociale existe deja! voulez vous modifiez un patient? si oui saisissez vos informations sans changer le numéro de secu"
          }
          i++;
        }

      }
/*
  public infDontExiste(infirmier :InfirmierInterface) : boolean{
    let i : number = 0;

    while (this._cabinet.infirmiers.length >i){
      if(this._cabinet.infirmiers[i].nom == infirmier.nom && this._cabinet.infirmiers[i].prénom == infirmier.prénom &&
        this._cabinet.infirmiers[i].id == infirmier.id){
        return false;
      }
      i++;
    }
    return true;
  }*/

  affectedOrNot(item){// si le patient est non affecté on revoie true
    let i : number = 0;

    while (this._cabinet.patientsNonAffectés.length >i){
      if(this._cabinet.patientsNonAffectés[i].nom == item.nom && this._cabinet.patientsNonAffectés[i].prénom == item.prénom){
        return true;
      }
      i++;
    }
    return false;
  }


  public ajouter(patient: PatientInterface,anniv : string) {
    return new Promise(
      (resolve,reject)=> {
        this._http.post('/addPatient', {
          patientForname: patient.prénom,
          patientName: patient.nom,
          patientSex: (patient.sexe===0)? "M" : "F",
          naissance: anniv,
          patientNumber: patient.numéroSécuritéSociale,
          visiteDate:patient.visiteDate,
          patientCity: patient.adresse.ville,
          patientPostalCode: patient.adresse.codePostal,
          patientStreet: patient.adresse.rue,
          patientStreetNumber: patient.adresse.numéro,
          patientFloor: patient.adresse.étage
        }).toPromise().then(
          () => {
            resolve();
          },
          err => {
            reject(err);
          }
        );
      }
    );
  }
  public affecter(index : string ,infirmierId : string) {
    //let infirmierId: string = infirmier;
    const patient = this._cabinet.patients.find(
      (s) => {
        return s.numéroSécuritéSociale === index;
      }
      );
    return new Promise(
      (resolve,reject)=> {
        this._http.post('/affectation', {
          infirmier: infirmierId,
          patient: patient.numéroSécuritéSociale
        }).toPromise().then(
          () => {
            resolve();
          },
          err => {
            reject(err);
          }
        );
      }
    );
  }
  /*---------------desAnffecter :methode qui permet d'enlever l'intervenant d' un patient------------------------- */
  public desAffecter(index : string  ) {
    const patient = this._cabinet.patients.find(
      (s) => {
        return s.numéroSécuritéSociale === index;
      }
    );
    return new Promise(
      (resolve,reject)=> {
        this._http.post('/affectation', {
          infirmier: 'none',
          patient: patient.numéroSécuritéSociale
        }).toPromise().then(
          () => {
            resolve();
          },
          err => {
            reject(err);
          }
        );
      }
    );
  }
/*--------------------trouver le patient avec son num de secu pour le modifier dans edit-patient , et aussi pour recuperer ces info dans edit pat*/

  getAppareilByNS(ns: string):PatientInterface {
    const patient = this._cabinet.patients.find(
      (s) => {
        return s.numéroSécuritéSociale === ns;
      }
    );
    return patient;
  }
  /*---------------verifier si le nom qu on vient de saisir existe deja (pour la modification)*/
  checkBeforModification(patient :PatientInterface,numSec : string) : boolean{
    let i : number = 0;
    let pat =this.getAppareilByNS(numSec);
    let nom:string = pat.nom;
    let prenom : string = pat.prénom;
    while (this._cabinet.patients.length >i){
      if(patient.prénom=== prenom && patient.nom === nom){
        return true;// on compare pas avec le nom du patient qu'on veut modifier
      }
      if(this._cabinet.patients[i].nom === patient.nom && this._cabinet.patients[i].prénom === patient.prénom ){
        return false;
      }
      i++;
    }
    return true;
  }
  /*-------------------- message d'erreur si le nom du patient existe deja (pour la modification)*/
  namePatientExiste(patient :PatientInterface) : string{
    let i : number = 0;

    while (this._cabinet.patients.length >i){
      if(this._cabinet.patients[i].nom == patient.nom && this._cabinet.patients[i].prénom == patient.prénom){
        return "le nom et le prénom que vous venez de saisir existe deja";
      }
      i++;
    }

  }
  public searchPat(sp: string){
    this.patientsRes=[];
    let i : number = 0;
   let pat :PatientInterface[]=[];
    while(this._cabinet.patients.length > i){
      let preNom : string = this._cabinet.patients[i].prénom+" "+this._cabinet.patients[i].nom;
      let nomPre : string = this._cabinet.patients[i].nom+" "+this._cabinet.patients[i].prénom;
      console.log(nomPre);
      if( this._cabinet.patients[i].prénom === sp || this._cabinet.patients[i].nom === sp || sp=== preNom || sp=== nomPre ){
        pat.push(this._cabinet.patients[i]);
      }
      i++;
    }
    this.patientsRes=pat;
    console.log(this.patientsRes);
    //location.reload(true);
    return pat;
  }
}
