import { Component } from '@angular/core';
import {CabinetInterface} from './dataInterfaces/cabinet';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CabinetMedicalService} from './cabinet-medical.service';
import {Router} from '@angular/router';
import {PatientInterface} from './dataInterfaces/patient';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  searchForm:FormGroup;
  private _cms: CabinetInterface;
  public get patients(): CabinetInterface { return this._cms; }
  constructor(private formBuilder: FormBuilder ,private cabinetMedicalService: CabinetMedicalService, private rout : Router) {

  }
  ngOnInit() {
    this.initFormInter();
  }
  /*-----------------------------------------------------------------------------------------------------------------*/
  initFormInter()
  {
    this.searchForm = this.formBuilder.group({
      search: ''
    });
  }

 openNav() {
    document.getElementById("mySidenav").style.width  = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  search(){
    this.rout.navigate(['/patients']);

    const formValue = this.searchForm.value;// recuperer les infos du formulaires(la valeur de chaque  champs remplis)
    let sp : string =formValue['search'];
    console.log(sp);
    this.cabinetMedicalService.searchPat(sp);
    //console.log(this.cabinetMedicalService.searchPat(sp));
    setTimeout(
      () => {
      }, 4000
    );
    this.rout.navigate(['/search']);
  }

}
