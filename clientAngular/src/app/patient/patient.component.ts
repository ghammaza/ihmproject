import { Component,Input ,OnInit } from '@angular/core';
import {PatientInterface} from '../dataInterfaces/patient';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  interForm:FormGroup;
  errorMessage : string ;
  private _cms: CabinetInterface;
  public get patients(): CabinetInterface { return this._cms; }
  constructor(private formBuilder: FormBuilder ,private cabinetMedicalService: CabinetMedicalService, private rout : Router) {
    this.initCabinet(cabinetMedicalService);

  }


  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    console.log( this.patients);
  }
  ngOnInit() {
    this.initFormInter();
  }
  /*-----------------------------------------------------------------------------------------------------------------*/
  initFormInter()
  {
    this.interForm = this.formBuilder.group({
      intervenant: ''
    });
  }
  /*-------------------------------mrthodes permetent de recuperer les infos du patient ou infirmier comme le nom ...----*/
  getNomPat(item){// recuperer le nom et le prenom
    return item.nom +" "+item.prénom;
  }

  getNum(item){// recuperer le numéro du domocile , si ca existe pas on renvoie :"pas de numéro"
    if(item.numéro !="") {
      return item.numéro;
    }else{
      return " pas de numéro";
    }
  }

  getEt(item){// recuperer l'etage du domicile , si ca existe pas on renvoie :"pas d'etage"
   if(item.étage !=""){
     return item.étage;
   }else {
     return " pas d'étage";
   }
  }

  getSexe(item){// recuperer le sexe d'un patient
    if(item.sexe===1){
      return " Femme";
    }
    else{
      return "Homme";
    }
  }

  getColorE(item){// si il n y a pas d'etage , on revoie la couleur rouge
    if( item.étage === "" ){
      return 'red';
    }
    return null;
  }
  getColorN(item){// meme chose que getColorE mais cette fois pour le numerp du domicile
    if(item.numéro === "" ){
      return 'red';
    }
    return null;
  }
  getNumSsecu(item){
    return item.numéroSécuritéSociale;
  }
  /*----------------------------------getInfirmier---------------------------------------------------------------------*/

  getInfirmiere(item) : string {// renvoie l'intervenant du patient desgigner en item
    let i : number=0;
    let infirmiere = this.patients.infirmiers;
    while(infirmiere.length>i){// chercher l'intervenant du patient en verifiant tous les patients de celui-ci
      let j : number=0;
      while (infirmiere[i].patients.length > j ){
        if(infirmiere[i].patients[j].prénom == item.prénom && infirmiere[i].patients[j].nom == item.nom){
          return infirmiere[i].prénom+" "+infirmiere[i].nom ;
        }
        j++;
      }
      i++;
    }
    return null ;
  }

  /*----------------------------------MyFActure : la facture client -------------------------------------------------*/
   myFacture(index:number) {
    let text : string ;
    let totalFacture : number=0;
    text= "<html>\n";
     text += "    <head>\n\
   <img src='assets/images/hfg.png'/><br>\n\
       <title>Facture de "+ this.patients.patients[index].nom+" " +this.patients.patients[index].prénom+" </title>\n\
            <link rel='stylesheet' type='text/css' href='./patient.component.scss'/>\n\
         </head>\n\
         <body class='body'>\n\
	  <h2>";
     text += "Facture pour " + this.patients.patients[index].nom + " " + this.patients.patients[index].prénom ;
     text +="</h2>";
     text += "Adresse: ";
     text += "<br/>";
     text +="étage"+this.getEt(this.patients.patients[index].adresse)+","+this.getNum(this.patients.patients[index].adresse) +"  "+this.patients.patients[index].adresse.rue+""+this.patients.patients[index].adresse.ville+"-"+this.patients.patients[index].adresse.codePostal;
     text += "<br/>";
     text += "Numéro de sécurité sociale: " + this.patients.patients[index].numéroSécuritéSociale+ "\n";
     text += "<br/>";
     //l'intervenant du patient
     text += "Intervenant: " + this.getInfirmiere(this.patients.patients[index])+ "\n";
     text += "<br/>";
     // Tableau rÃ©capitulatif des Actes et de leur tarif
     text += "<table class='table1'   style='border:3;'>";
     //text += "<trstyle='border:2;'>";
     text += "<tr>";
     text+=" <th rowspan='1' colspan='5' style='background-color: red; color: white; border-collapse: collapse;border: 2px solid black;'>Toutes les actes</th>"
     text += "</tr>";
     text += "<tr>";
     text += "<th style='background-color: #dddddd;'> Type </th> <th style='background-color: #dddddd;'> Clé </th> <th style='background-color: #dddddd;'> Intitulé </th> <th style='background-color: #dddddd;'> Coef </th> <th style='background-color: #dddddd;'> Tarif </th>";
     text += "</tr>";
     // a remplir

     text += "<tr><td rowspan='1' colspan='4'style='background-color: black; color: white; border-collapse: collapse;border: 2px solid black;'>Total</td><td style='background-color: black; color: white; border-collapse: collapse;border: 2px solid black;'>" + totalFacture + " €</td></tr>\n";
     text +="</h3>";
     text +="</table>";
     text +=
       "    </body>\n\
</html>\n";
    var myWindow= window.open("", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
     myWindow.document.write(text);
  }


  /*----------------------------------patDet:detail du patient-------------------------------------------------------- */


// cette fonction permet d'ouvrir une fenetre qui contient les details d'un patient , cette fonction prend en param l'index du patient
  patDet(index:number) {
    let text : string ;
    text= "<html>\n";
    text += "    <head>\n\
   <img src='assets/images/hfg.png'/><br>\n\
       <title>Detail de "+ this.patients.patients[index].nom+" " +this.patients.patients[index].prénom+" </title>\n\
            <link rel='stylesheet' type='text/css' href='./patient.component.scss'/>\n\
         </head>\n\
         <body class='body'>\n\
	  <h2>";

    // recuperer les infos du patient
    text += "Detail de " + this.patients.patients[index].nom + " " + this.patients.patients[index].prénom ;// nom est prénom
    text +="</h2>";
    text += "Adresse: ";
    text += "<br/>";
    //adresse du client
    text +="étage"+this.getEt(this.patients.patients[index].adresse) +","+this.getNum(this.patients.patients[index].adresse) +"  "+this.patients.patients[index].adresse.rue+""+this.patients.patients[index].adresse.ville+"-"+this.patients.patients[index].adresse.codePostal;
    text += "<br/>";
    // numero de secu
    text += "Numéro de sécurité sociale: " + this.patients.patients[index].numéroSécuritéSociale+ "\n";
    text += "<br/>";
    //l'intervenant du patient
    text += "Intervenant: " + this.getInfirmiere(this.patients.patients[index])+ "\n";
    text += "<br/>";
    // Tableau rÃ©capitulatif des Actes et de leur tarif
    text += "<table class='table1'   style='border:3;'>";
    text += "<tr style='border:2;'>";
   // text += "<tr>";
    text+=" <th rowspan='1' colspan='5' style='width:200px;background-color: red; color: white; border-collapse: collapse;border: 2px solid black;'>Date de visite</th>"
    text += "</tr>";
   /* for(let i : number ;this.patients.patients[index].visite.length > i ; i++){
      text+="<tr>"+this.patients.patients[index].visite[i].date+"</tr>";
    }*/

    text+="<h2><td>"+this.patients.patients[index].visiteDate+"</td></h2>";
    // a remplir
    text += "<tr>";
    text += "</tr>";
    text +="</table>";
    text +=
      "    </body>\n\
</html>\n";
    var myWindow= window.open("", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    myWindow.document.write(text);



  }
  // verifier si le patient n est pas affecter a une infirrmiere , cette methode est utiliseés dans le templatate pour afficher ou pas le input d'affectation
  notAffected(item){
    return this.cabinetMedicalService.affectedOrNot(item);
  }

  affectePatient(index : string){
    const formValue = this.interForm.value;// recuperer les infos du formulaires(la valeur de chaque  champs remplis)
    let infirmier : string  = formValue['intervenant'];
    if (infirmier ===''){
      infirmier ='001';
    }
    //var x = document.forms["myForm"]["intervenant"].value;
    console.log(infirmier);
    this.cabinetMedicalService.affecter(index,infirmier).then(
      ()=>{
        location.reload(true);
      },
    (error)=>{
        this.errorMessage=error;
    }
    );
  }
  /*---------------deAffecterpatient : permet d'enlever l'intervenant d'un pat en utilisant la methode deAffecter du service*/
  desAffectePatient(index : string){

    this.cabinetMedicalService.desAffecter(index).then(
      ()=>{
        location.reload(true);
      },
      (error)=>{
        this.errorMessage=error;
      }
    );
  }

}
